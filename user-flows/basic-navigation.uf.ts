// Your custom interactions with the page
import {
  UserFlowContext,
  UserFlowInteractionsFn,
  UserFlowProvider,
} from "@push-based/user-flow";


const delay = async (ms: number) => new Promise(r => setTimeout(r, ms));

const interactions: UserFlowInteractionsFn = async (ctx: UserFlowContext): Promise<void> => {
  const { flow, collectOptions, page} = ctx;
  const { url } = collectOptions;

  await flow.navigate(async () => {
    await page.goto(url);
    await delay(8_000);
  }, {
    stepName: `Cold navigation to ${url}`,
  });

  await flow.navigate(async () => {
    await page.goto(url);
    await delay(8_000);
  }, {
    stepName: `Warm navigation to ${url}`,
  });
};

const userFlowProvider: UserFlowProvider = {
  launchOptions: { headless: "new", args: ['--no-sandbox'] },
  flowOptions: { name: "Basic Navigation Example" },
  interactions,
};

module.exports = userFlowProvider;